//担当者：山田
#pragma once
#include "Engine/Global.h"
#include "Engine/Input.h"
#include "Engine/Base.h"
#include "PlayScene.h"


class ResultScene :
	public Base
{
private:
	//シーンに必要な画像の読み込み
	cv::Mat imgPlay_	= cv::imread(IMG_PLAY, 1);
	cv::Mat imgBack_	= cv::imread(IMG_BACK);
	cv::Mat imgReturn_	= cv::imread(IMG_RETURN, 1);
	cv::Mat imgRetry_	= cv::imread(IMG_RETRY, 1);
	cv::Mat imgComment_ = cv::imread(IMG_COMMENT, 1);
	cv::Mat imgHome_	= cv::imread(IMG_HOME, 1);
	cv::Mat imgTurtle_	= cv::imread(IMG_TURTLE, 1);
	cv::Mat imgTheme_	= cv::imread(IMG_THEME, 1);
	cv::Mat imgRabbit_	= cv::imread(IMG_RABBIT, 1);

	//各UIの位置、大きさ
	const UiData backButton_ = {   30,   30,  150,  150 };
	const UiData retry_		 = { 1550,  830,  300,  150 };
	const UiData illust_	 = {  200,  200, 1024, 1024 };
	const UiData comment_	 = {  200,  750,  800,  360 };
	const UiData batch_		 = { 1430,  200,  400,  300 };

	//イラストの評価 *千田
	//戻値：評価コメントのフルパス
	std::string EvaluateIllust();

public:
	ResultScene();
	ResultScene(Base * parent);
	~ResultScene();

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};