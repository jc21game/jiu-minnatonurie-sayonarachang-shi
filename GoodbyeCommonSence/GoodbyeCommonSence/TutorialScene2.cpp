//　担当者　奥山　,  伊藤

#include "Engine/SceneManager.h"//シーンマネージャー
#include "Engine/Global.h"//画像
#include "TutorialScene2.h"

//コンストラクタ
TutorialScene2::TutorialScene2(Base * parent)
	:Base(parent, "TutorialScene2")
{
}

//初期処理
void TutorialScene2::Initialize()
{
}

//更新処理
void TutorialScene2::Update()
{
	//左クリックでスキップ
	//キーボード長押しで連続でシーン切り替え可能だったため変更
	if (Input::MouseLDown())
	{
		//セレクトシーン遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_SELECT);

	}
}

void TutorialScene2::Draw()
{
	//描画
	imshow(WIN_NAME, imgTutorial2_);

}

//解放処理
void TutorialScene2::Release()
{
	imgTutorial2_.release();

}
