//担当者：山田
#include <direct.h>
#include "Engine/SceneManager.h"
#include "Engine/Global.h"
#include "Engine/Input.h"
#include "Engine/Text.h"
#include "Engine/Data.h"
#include "ResultScene.h"
#include "PlayScene.h"

ResultScene::ResultScene()
{
}

ResultScene::ResultScene(Base* parent)
	: Base(parent, "ResultScene")
{
}

ResultScene::~ResultScene()
{
}

void ResultScene::Initialize()
{
	//マウスセット
	cv::setMouseCallback(WIN_NAME, Input::mouse_callback, &mouseEvent);

	//保存したイラストのパスを入れる文字列
	cv::String savedImg = Data::path;
	//保存パスからMat作成
	cv::Mat imgCompleted = cv::imread(savedImg, 1);

	//プレイ時間が5分以上だったら遅い判定
	int timeScore = Data::playTime;
	if (timeScore > 300) imgPlay_ = JoinImage(imgPlay_, imgTurtle_, batch_.POS_X, batch_.POS_Y);
	else imgPlay_ = JoinImage(imgPlay_, imgRabbit_, batch_.POS_X, batch_.POS_Y);

	imgPlay_ = JoinImage(imgPlay_, imgReturn_, backButton_.POS_X, backButton_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgRetry_, retry_.POS_X, retry_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgComment_, comment_.POS_X,comment_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgCompleted, illust_.POS_X, illust_.POS_Y);	
	imgPlay_ = JoinImage(imgPlay_, imgTheme_, 200, 45);

	//お題表示
	Text text;
	text.SetDraw(imgPlay_, text.line_[text.lineNumber_], cv::Point(240, 100), "メイリオ", 10, cv::Scalar(0, 0, 0));
	text.Indention(Data::GetThemePath(Data::illustNumber));

	//時間表示
	cv::putText(imgPlay_,
		"time :" + std::to_string(timeScore) + " second",
		cv::Point(1300, 150),
		cv::FONT_HERSHEY_COMPLEX, 2,
		cv::Scalar(0, 0, 0),
		3);

	//コメント表示
	//=>評価コメントはMSゴシック、サイズ7で1行当たり横10文字までになります。
	//  フォントによって大きさが異なるので注意です。	*千田
	text.SetDraw(imgPlay_, text.line_[text.lineNumber_], cv::Point(240, 820), "HG明朝B", 8, cv::Scalar(255, 255, 255));
	text.Indention(EvaluateIllust());

	//一応、完成したイラストも解放
	//imgCompleted.release();
}

void ResultScene::Update()
{
	//左クリックされた時のマウスの座標セット
	if (Input::MouseLDown())
	{
		SetMousePos();
	}

	cv::Rect backButtonCollider = cv::Rect(backButton_.POS_X, backButton_.POS_Y, backButton_.SIZE_X, backButton_.SIZE_Y);
	//戻るボタンをクリックしたら、画像選択シーンに遷移
	if (backButtonCollider.contains(mouse))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_SELECT);
	}

	cv::Rect retryButtonCollider = cv::Rect(retry_.POS_X, retry_.POS_Y, retry_.SIZE_X, retry_.SIZE_Y);
	//再挑戦ボタンをクリックしたら、プレイシーンに遷移
	if (retryButtonCollider.contains(mouse))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

void ResultScene::Draw()
{
	//画像表示
	cv::imshow(WIN_NAME, imgPlay_);
}

void ResultScene::Release()
{
	//matのリリース機能
	imgTheme_.release();
	imgTurtle_.release();
	imgHome_.release();
	imgComment_.release();
	imgRetry_.release();
	imgReturn_.release();
	imgBack_.release();
	imgPlay_.release();
}

//イラストの評価を行う
std::string ResultScene::EvaluateIllust()
{
	//パスの色部分
	std::string commentColor;

	//もっとも使われた色に応じて分岐
	switch (Data::passingColor)
	{
	case RED:
		commentColor = "Red";	 break;
	case ORANGE:
		commentColor = "Orange"; break;
	case YELLOW:
		commentColor = "Yellow"; break;
	case GREEN:
		commentColor = "Green";  break;
	case BLUE:
		commentColor = "Blue";	 break;
	case PURPLE:
		commentColor = "Purple"; break;
	case PINK:
		commentColor = "Pink";   break;
	case BROWN:
		commentColor = "Brown";	 break;
	case GRAY:
		commentColor = "Gray";   break;
	case WHITE:
		commentColor = "White";  break;
	}

	//全体パスの結合
	std::string fullCommentPath = "Text/Comment/" + Data::GetCommentPath(Data::illustNumber) + commentColor + ".txt";

	//テキスト出力に必要なパスを返す
	return fullCommentPath;
}