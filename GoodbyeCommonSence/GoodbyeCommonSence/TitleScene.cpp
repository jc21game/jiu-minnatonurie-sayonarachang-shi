#include "Engine/SceneManager.h"
#include "Engine/Global.h"
#include "Engine/Text.h"
#include "TitleScene.h"

#include <opencv2/videoio.hpp>
//#include <Windows.h>

//音声再生のための依存ファイル
//#pragma comment(lib, "Winmm.lib")

//コンストラクタ
TitleScene::TitleScene(Base* parent)
	: Base(parent, "TitleScene")
{
}

//初期処理
void TitleScene::Initialize()
{
	imgBack_ = JoinImage(imgBack_, imgColorTitle_, 448, 27);		//元絵配置

	// 動画ファイルのパスの文字列を格納するオブジェクトを宣言する
	std::string filePath = "Texture/nurie_titlemovie_fixed.mp4";

	// 動画ファイルを開く
	video_.open(filePath);

	// 動画ファイルが開けなかったときは終了する
	if (video_.isOpened() == false)
	{
		cv::destroyAllWindows();
	}
}



//更新処理
void TitleScene::Update()
{
	//少しずつ流れるように表示
	//リリースモードのほうが滑らかに動きます
	//Press〜表示
	cv::putText(imgBack_,
		"Press LeftMouse",
		cv::Point(650, 850),
		cv::FONT_HERSHEY_COMPLEX, 2,
		cv::Scalar(0, 0, 0),
		3);

	//左クリックでスキップ
	//キーボード長押しで連続でシーン切り替え可能だったため変更
	if (Input::MouseLDown())
	{
		//シーン遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TUTORIAL);
	}


}

void TitleScene::Draw()
{
	//描画
	//cv::imshow(WIN_NAME, imgBack_);

	/* 以下動画ver */
	// videoからimageへ1フレームを取り込む
	video_.read(movie_);

	//音声再生
	//PlaySound("Texture/nurie_titlemovie_fixed.wav", NULL, SND_FILENAME | SND_ASYNC);


	//動画が最後まで再生されたら
	if (movie_.empty())
	{
		//少し待つ
		//cv::waitKey(1000);

		cv::imshow(WIN_NAME, imgBack_);

		//PlaySound(NULL, NULL, 0);
	}
	else
	{
		//動画の再生
		cv::imshow(WIN_NAME, movie_); 
	}
}

//解放処理
void TitleScene::Release()
{
	//MatのRelease機能
	video_.release();
	movie_.release();
	imgBack_.release();
	imgColorTitle_.release();
}