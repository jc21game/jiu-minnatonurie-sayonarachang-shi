/* チュートリアルシーン */

#pragma once
#include "Engine/Global.h"
#include "Engine/Base.h"
class TutorialScene2 : public Base
{
	//画像名
	cv::Mat imgTutorial2_ = cv::imread(IMG_TUTORIAL_2, 1);

public:
	//コンストラクタ
	TutorialScene2(Base* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};