/*
*
*エンジン作成		担当者：千田紘生
*							奥山
*
*
*/
//警告の削除
#pragma warning(disable : 4819)

//open_cvの基本的な機能
#include <opencv2\highgui.hpp> //ウィンドウ・画像関連
#include <opencv2\opencv.hpp>
#include <Windows.h>
#include <stdlib.h>

#include "Engine/RootJob.h"
//#include "Engine/Input.h"
#include "Engine/Global.h"
#include "Engine/Timer.h"

#pragma comment(lib,"winmm.lib")

//インスタンス
RootJob* pRootJob = new RootJob;

int main()
{
	//ウィンドウ
	//引数：ウィンドウ名、ウィンドウフラグ(0:可変 1:不変)
	cv::namedWindow(WIN_NAME);
	//ウィンドウの位置を左端に合わせる
	cv::moveWindow(WIN_NAME, 0, 0);

	//初期化処理
	pRootJob->Initialize();

	//画面サイズをフルスクリーンぴったりにする
	//SendMessage(GetActiveWindow(), WM_SYSCOMMAND, SC_MAXIMIZE, NULL);

	//メッセージループ(仮)
	MSG msg;
	//初期化(のようなもの)
	ZeroMemory(&msg, sizeof(msg));

	while (msg.message != WM_QUIT)
	{

		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		else
		{
			//経過時間更新
			Timer::UpdateFrameDelta();

			static double nowTime = 0;
			nowTime += Timer::GetDelta();
			static double lastUpdateTime = nowTime;

			if ((nowTime - lastUpdateTime) * 60 <= 1)
			{
				continue;
			}
			lastUpdateTime = nowTime;

			/* 以下ゲーム処理 */
			pRootJob->UpdateSub();

			pRootJob->DrawSub();
			//Escキーで強制終了 *千田
			if (GetAsyncKeyState(VK_ESCAPE) & 1)
			{
				//解放処理
				pRootJob->ReleaseSub();
				delete (pRootJob);
				pRootJob = nullptr;

				//ウィンドウ破棄
				cv::destroyAllWindows();
				return 0;			
			}


			//少し休ませる
			cv::waitKey(10);
		}
	}

	//解放処理
	pRootJob->ReleaseSub();
	delete (pRootJob);
	pRootJob = nullptr;

	//ウィンドウ破棄
	PostQuitMessage(0);
	cv::destroyAllWindows();

	return 0;
}

//プッシュテスト