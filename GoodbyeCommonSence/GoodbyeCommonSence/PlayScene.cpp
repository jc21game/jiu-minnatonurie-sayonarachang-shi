#include <opencv2/highgui.hpp>
#include <direct.h>
#include <time.h>

#include "Engine/SceneManager.h"
#include "Engine/Global.h"
#include "Engine/Input.h"
#include "Engine/Timer.h"
#include "Engine/Data.h"
#include "PlayScene.h"
//#include <Windows.h>
/*  追記　*千田
	Window.hをインクルードすることで名前空間の参照があいまいになり、
	エラーが起こったので注意です。
	=>今はnamespace cvを使ってないので大丈夫だと思います。
*/

//コンストラクタ
PlayScene::PlayScene(Base* parent)
	: Base(parent, "PlayScene"),startTime_(std::chrono::system_clock::now())
{
}

//初期処理
void PlayScene::Initialize()
{
	//マウスセット
	cv::setMouseCallback(WIN_NAME, Input::mouse_callback, &mouseEvent);

	//イラスト
	//遊ぶイラストはセレクトシーンで受け取ったイラスト番号から判断
	imgIllust_ = Data::GetIllustMat(Data::illustNumber);
	resize(imgIllust_, imgIllust_, cv::Size(), 700.0 / imgIllust_.cols, 700.0 / imgIllust_.rows);

	//画像統合
	imgPlay_ = JoinImage(imgPlay_, ImageThresholding(imgIllust_), illust_.POS_X, illust_.POS_Y);	//イラストを二値化してから統合
	imgPlay_ = JoinImage(imgPlay_, imgPallet_, palette_.POS_X, palette_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgHome_, home_.POS_X, home_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgComplete_, complete_.POS_X, complete_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, ImageThresholding(imgBucket_), bucket_.POS_X, bucket_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgChangeMode_, changeMode_.POS_X, changeMode_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgSaveParet_, saveParet_.POS_X, saveParet_.POS_Y);
	imgPlay_ = JoinImage(imgPlay_, imgTheme_, theme_.POS_X, theme_.POS_Y);

	//お題表示
	Text text;
	text.SetDraw(imgPlay_, text.line_[text.lineNumber_], cv::Point(590, 100), "メイリオ", 10, cv::Scalar(0, 0, 0));
	text.Indention(Data::GetThemePath(Data::illustNumber));

	//色を何も選択していないときに、黒色で塗りつぶせてしまうため、白色のBGRコードで初期化
	for (int i = 0; i < 3; i++)
	{
		pixData_[i] = 255;
	}

	InitializeColor();		//色定義の初期化
}

//更新処理
void PlayScene::Update()
{
	//プレイシーンを自主的に終了した後もシーン遷移がかなってしまうので要修正
	//マウス処理
	if (Input::MouseLDown())
	{
		SetMousePos();
		//モード切替ボタン当たり判定
		cv::Rect spuitCollider = cv::Rect(changeMode_.POS_X, changeMode_.POS_Y, changeMode_.SIZE_X, changeMode_.SIZE_Y);
		if (spuitCollider.contains(mouse))
		{
			mode_ = mode_ < SPUIT ? SPUIT : PAINT;
		}

		//ホームボタンをクリックしたらセレクトシーンに移動
		cv::Rect homeCollider = cv::Rect(home_.POS_X, home_.POS_Y, home_.SIZE_X, home_.SIZE_Y);
		if (homeCollider.contains(mouse))
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_SELECT);
		}

		//絵のあたり判定
		cv::Rect illustCollider = cv::Rect(illust_.POS_X, illust_.POS_Y, illust_.SIZE_X, illust_.SIZE_Y);

		if (mode_ == PAINT)
		{
			//モードに応じたアイコン表示
			imgPlay_ = JoinImage(imgPlay_, imgBucketMode_, changeMode_.POS_X, changeMode_.POS_Y);

			//もし、クリックした位置がパレット内だったら(右)	
			if (IsPalette(R_PAINTS_COUNT_))
			{
				pixData_ = GetPixDatas(mouse.x, mouse.y);
				DispSelectedColor();
				//白で無ければ3/1の値を入れる
				if (pixData_ != color_[WHITE].front())
				{
					pixData_ = pixData_ / 3;
				}
			}

			//もし、クリックした位置がパレット内だったら(左)	
			if (IsPalette(L_PAINTS_COUNT_))
			{
				pixData_ = GetPixDatas(mouse.x, mouse.y);
				DispSelectedColor();
			}

			//イラスト内なら
			if (illustCollider.contains(mouse) && !IsLineArt(mouse.x, mouse.y))
			{
				//白だったら所持してる色をそのまま塗る
				if (GetPixDatas(mouse.x, mouse.y) == color_[WHITE].front() || pixData_ == color_[WHITE].front())
				{
					FillWithColor(imgPlay_, mouse.x, mouse.y, pixData_);
				}
				//合成した色を塗る
				else
				{
					//ここで色を合わせる
					cv::Vec3b mixPixel = MixColor(pixData_, mouse.x, mouse.y);
					FillWithColor(imgPlay_, mouse.x, mouse.y, mixPixel);
				}
			}
		}

		else if (mode_ == SPUIT)
		{
			//モードに応じたアイコン表示
			imgPlay_ = JoinImage(imgPlay_, imgSpuitMode_, changeMode_.POS_X, changeMode_.POS_Y);

			//クリックした位置がイラスト内なら
			if (illustCollider.contains(mouse) && !IsLineArt(mouse.x, mouse.y))
			{
				saveColorData = GetPixDatas(mouse.x, mouse.y);	//色を格納
			}
			//パレット内なら
			else if (IsPalette(L_PAINTS_COUNT_))
			{
				FillWithColor(imgPlay_, mouse.x, mouse.y, saveColorData);
			}
		}

		//完成ボタンをクリックしたら、リザルトシーンに遷移
		cv::Rect completeButtonCollider = cv::Rect(complete_.POS_X, complete_.POS_Y, complete_.SIZE_X, complete_.SIZE_Y);
		if (completeButtonCollider.contains(mouse))
		{
			//終了時間確保
			auto nowTime = std::chrono::system_clock::now();

			//リザルトに色情報を渡す *千田
			GetMostUsedColor();

			//シーン遷移
			SceneManager* pSceneManeger = (SceneManager*)FindObject("SceneManager");
			//イラストを保存してパスをResultへ
			Cutout(imgPlay_, &Data::path);
			//遊んだ時間を記録
			Data::playTime = (int)Timer::EndPlayTime(startTime_, nowTime);
			pSceneManeger->ChangeScene(SCENE_ID_RESULT);
		}
	}
	//とりあえず右クリックでその時点の色の分布のヒストグラム画像を表示するようにしてます
	//不要ならコメントアウトしておｋです
	if (mouseEvent.event == cv::EVENT_RBUTTONDOWN)
	{
		//色判定
		GetMostUsedColor();
	}
}

void PlayScene::Draw()
{
	//画像表示
	imshow(WIN_NAME, imgPlay_);
}

//解放処理
void PlayScene::Release()
{
	//MatのRelease機能
	imgTheme_.release();
	imgSaveParet_.release();
	imgChangeMode_.release();
	imgBucket_.release();
	imgComplete_.release();
	imgHome_.release();
	imgPallet_.release();
	imgBack_.release();
	imgIllust_.release();
	imgPlay_.release();
}

//選択した座標の色情報を保持
cv::Vec3b PlayScene::GetPixDatas(int x, int y)
{
	//指定した座標の画素値を取得する
	cv::Mat3b ptMat = imgPlay_;
	return ptMat(cv::Point(x, y));
}

void PlayScene::FillWithColor(cv::Mat img, int startX, int startY, cv::Vec3b colorData)
{
	//塗りつぶし関数
	//引数: 画像,塗りつぶしの開始位置,色,オプション,比較する色の許容下限値,比較する色の許容上限値,比較する色の許容範囲
	floodFill(img,
		cv::Point(startX, startY),
		cv::Vec3b(colorData[0], colorData[1], colorData[2]),
		(cv::Rect*)0,
		cv::Vec3b(20, 20, 20),
		cv::Vec3b(20, 20, 20),
		cv::FLOODFILL_FIXED_RANGE);
}

//クリックした位置がパレット内かどうか
bool PlayScene::IsPalette(int paintsCount)
{
	int paintsInterval;		//絵具の中心座標の間隔
	int circleX;			//円の中心座標
	int circleY;
	//左右のあたり判定のスタート座標
	const int R_START_Y = 260;
	const int L_START_Y = 256;
	//右のパレットの当たり判定
	if (paintsCount == R_PAINTS_COUNT_)
	{
		paintsInterval = 105;
		circleX = 1615;
		circleY = 260;
	}
	//左のパレットの当たり判定
	else if (paintsCount == L_PAINTS_COUNT_)
	{
		paintsInterval = 93;
		circleX = 305;
		circleY = 256;
	}

	const int RADIUS = 35;
	for (int i = 0; i < paintsCount; i++)
	{
		//円の当たり判定の中心座標をずらし、判定を移動
		circleY += i * paintsInterval;
		//円の当たり判定
		int circle = ((mouse.x - circleX) * (mouse.x - circleX)) + ((mouse.y - circleY) * (mouse.y - circleY));
		int distance = RADIUS * RADIUS;
		//パレット内にいた。
		if (circle <= distance)
		{
			selectedPaintPosY_ = circleY;
			return true;
		}
		circleY = paintsCount < R_PAINTS_COUNT_ ? L_START_Y : R_START_Y;
	}
	//パレット内にいなかった
	return false;
}

//選んでいる色を表示
void PlayScene::DispSelectedColor()
{
	int formerColorRate = 3;	//パレット通りの色に戻すための倍率

	int bucketInsideX = 1800;	//バケツの中
	int bucketInsideY = 1000;
	int bucketOutsideX = 1860;	//バケツの外側の雫
	int bucketOutsideY = 1015;

	cv::Vec3b fillColor;
	if (IsPalette(R_PAINTS_COUNT_))
	{
		for (int i = 0; i < 3; i++)
		{
			fillColor[i] = pixData_[i] * formerColorRate;	//パレット通りの色に戻す
		}
	}

	for (int i = 0; i < 3; i++)
	{
		fillColor[i] = pixData_[i];
	}

	FillWithColor(imgPlay_, bucketInsideX, bucketInsideY, fillColor);
	FillWithColor(imgPlay_, bucketOutsideX, bucketOutsideY, fillColor);
}

//クリックした位置が線画かどうか
bool PlayScene::IsLineArt(int x, int y)
{
	cv::Mat3b ptMat = imgPlay_;
	if (ptMat(cv::Point(x, y))[0] <= 5 && ptMat(cv::Point(x, y))[1] <= 5 && ptMat(cv::Point(x, y))[2] <= 5)
	{
		return true;
	}
	return false;
}

//画像を指定領域で切り抜き(本来はリザルトに？) *千田
cv::Mat PlayScene::Cutout(cv::Mat mat , std::string* path)
{
	//現在時刻を戻値で返す
	time_t t = time(NULL);
	//const tm* localTime = localtime(&t);

	//画像切り抜き
	//左から 始点のx座標、y座標、切り抜く横幅、縦幅
	cv::Rect rect = cv::Rect(610, 190, 700, 700);

	cv::Mat out(mat, rect);
	resize(out, out, cv::Size(), 512.0 / out.cols, 512.0 / out.rows);
	//抜いた画像保存

	//保存先作成
	if (_mkdir("folder") == 0)
	{
		std::cout << "OK" << std::endl;
	}

	//画質を設定するparam(vectorで生成)
	std::vector<int> params(2);
	//型指定
	params[0] = cv::IMWRITE_JPEG_QUALITY;
	//画質指定
	params[1] = 100;
	//書き出し(保存)
	*path = "folder/test_" + std::to_string(t) + ".jpg";
	imwrite("folder/test_" + std::to_string(t) + ".jpg", out, params);

	return out;
}

//最も使われている色の判定 *櫻庭
void PlayScene::GetMostUsedColor()
{
	int mostUsedPix = 0; //一番使われている色のピクセル数	
	std::map<ColorPalette, int> pixCount; //それぞれの色が使われているピクセル数保存用

	//700x700の全ピクセルの色を取得、カウント
	for (int y = illust_.POS_Y, yMax = illust_.POS_Y + illust_.SIZE_Y; y < yMax; ++y)
	{
		for (int x = illust_.POS_X, xMax = illust_.POS_X + illust_.SIZE_X; x < xMax; ++x)
		{
			if (GetPixDatas(x, y) != color_[BLACK].front())
			{
				++pixCount[IdentifyColor(GetPixDatas(x, y))];
			}
		}
	}

	//それぞれの色のピクセル数表示
	for (int i = 0; i < COLORMAX; ++i)
	{
		std::cout << pixCount[(ColorPalette)i] << "\n";
	}

	//最も使われている色を取得
	for (auto itr = pixCount.begin(); itr != pixCount.end(); ++itr)
	{
		if ((*itr).second > mostUsedPix)
		{
			mostUsedPix = (*itr).second;
			mostUsedColor_ = (*itr).first;
		}
	}
	
	//評価を表示
	//=>デバックでプレイ中も確認できるよう、残しています。	*千田
	std::cout << "評価 : " << EvaluateColor() << "\n";

	//もっとも使われた色をリザルトに渡す
	Data::passingColor = mostUsedColor_;
}

//色の初期化
void PlayScene::InitializeColor()
{
	//色名でBGRを登録
	color_[RED].emplace_back(0, 0, 255);
	color_[RED].emplace_back(0, 0, 170);

	color_[PINK].emplace_back(85, 0, 255);
	color_[PINK].emplace_back(85, 85, 255);
	color_[PINK].emplace_back(170, 0, 255);
	color_[PINK].emplace_back(170, 85, 255);
	color_[PINK].emplace_back(170, 170, 255);
	color_[PINK].emplace_back(255, 0, 255);
	color_[PINK].emplace_back(255, 85, 255);
	color_[PINK].emplace_back(255, 170, 255);

	color_[ORANGE].emplace_back(0, 85, 255);
	color_[ORANGE].emplace_back(0, 170, 255);
	color_[ORANGE].emplace_back(85, 170, 255);

	color_[YELLOW].emplace_back(0, 170, 170);
	color_[YELLOW].emplace_back(0, 255, 255);
	color_[YELLOW].emplace_back(85, 255, 255);
	color_[YELLOW].emplace_back(170, 255, 255);

	color_[GREEN].emplace_back(0, 85, 0);
	color_[GREEN].emplace_back(0, 85, 85);
	color_[GREEN].emplace_back(0, 170, 0);
	color_[GREEN].emplace_back(0, 170, 85);
	color_[GREEN].emplace_back(0, 255, 0);
	color_[GREEN].emplace_back(0, 255, 85);
	color_[GREEN].emplace_back(0, 255, 170);
	color_[GREEN].emplace_back(85, 85, 0);
	color_[GREEN].emplace_back(85, 170, 0);
	color_[GREEN].emplace_back(85, 170, 85);
	color_[GREEN].emplace_back(85, 170, 170);
	color_[GREEN].emplace_back(85, 255, 0);
	color_[GREEN].emplace_back(85, 255, 85);
	color_[GREEN].emplace_back(85, 255, 170);
	color_[GREEN].emplace_back(170, 255, 0);
	color_[GREEN].emplace_back(170, 255, 85);
	color_[GREEN].emplace_back(170, 255, 170);

	color_[BLUE].emplace_back(85, 0, 0);
	color_[BLUE].emplace_back(170, 0, 0);
	color_[BLUE].emplace_back(170, 85, 0);
	color_[BLUE].emplace_back(170, 85, 85);
	color_[BLUE].emplace_back(170, 170, 0);
	color_[BLUE].emplace_back(170, 170, 85);
	color_[BLUE].emplace_back(255, 0, 0);
	color_[BLUE].emplace_back(255, 85, 0);
	color_[BLUE].emplace_back(255, 85, 85);
	color_[BLUE].emplace_back(255, 170, 0);
	color_[BLUE].emplace_back(255, 170, 85);
	color_[BLUE].emplace_back(255, 255, 0);
	color_[BLUE].emplace_back(255, 255, 85);
	color_[BLUE].emplace_back(255, 255, 170);

	color_[PURPLE].emplace_back(85, 0, 85);
	color_[PURPLE].emplace_back(85, 0, 170);
	color_[PURPLE].emplace_back(170, 0, 85);
	color_[PURPLE].emplace_back(170, 0, 170);
	color_[PURPLE].emplace_back(170, 85, 170);
	color_[PURPLE].emplace_back(255, 0, 85);
	color_[PURPLE].emplace_back(255, 0, 170);
	color_[PURPLE].emplace_back(255, 85, 170);
	color_[PURPLE].emplace_back(255, 170, 170);

	color_[BROWN].emplace_back(0, 85, 170);
	color_[BROWN].emplace_back(0, 0, 85);
	color_[BROWN].emplace_back(85, 85, 170);

	color_[GRAY].emplace_back(85, 85, 85);
	color_[GRAY].emplace_back(170, 170, 170);

	color_[WHITE].emplace_back(255, 255, 255);

	color_[BLACK].emplace_back(0, 0, 0);
}

//二値化処理
cv::Mat PlayScene::ImageThresholding(cv::Mat mat)
{
	//閾値の値
	int thresh = 150;
	//二値化する関数(opencvの関数)
	threshold(mat, mat, thresh, 255, cv::THRESH_BINARY);

	return mat;
}

//色の評価
cv::String PlayScene::EvaluateColor()
{
	//ここで判定の緩さを決める
	switch (mostUsedColor_)
	{
	case RED :
		return "情熱的な色だね！";
	case ORANGE :
		return "元気が溢れている色合いだね！";
	case YELLOW :
		return "うわっ眩しい！輝いてるね！";
	case GREEN :
		return "癒される出来栄えだね！";
	case BLUE :
		return "爽やかな色だね〜！";
	case PURPLE :
		return "神秘的な色合いだね！";
	case PINK :
		return "PINK";
	case BROWN :
		return "BROWN";
	case GRAY :
		return "しっかり色を塗ろう！";
	case WHITE : 
		return "しっかり色を塗ろう！";
	}

	return "ERROR!!";
}

//一番使われている色の判別
ColorPalette PlayScene::IdentifyColor(cv::Vec3b judgeColor)
{
	for (auto&& col : color_)
	{
		for (auto&& c : col.second)
		{
			if (c[0] == judgeColor[0] && c[1] == judgeColor[1] && c[2] == judgeColor[2])
				return col.first;
		}
	}
}

//色の合成
cv::Vec3b PlayScene::MixColor(cv::Vec3b mixColor, int x, int y)
{
	cv::Vec3b mixedColor = GetPixDatas(x, y);

	for (int i = 0; i < 3; i++)
	{
		//色が255を超えた場合頭打ち
		mixColor[i] = mixColor[i] + mixedColor[i] > UCHAR_MAX ? UCHAR_MAX : mixColor[i] + mixedColor[i];
	}

	return mixColor;
}