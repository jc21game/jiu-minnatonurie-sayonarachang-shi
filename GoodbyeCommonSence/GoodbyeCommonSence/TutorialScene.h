/* チュートリアルシーン */

#pragma once
#include "Engine/Global.h"
#include "Engine/Base.h"
class TutorialScene : public Base
{
	//画像名
	cv::Mat imgTutorial_ = cv::imread(IMG_TUTORIAL, 1);

public:
	//コンストラクタ
	TutorialScene(Base* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};