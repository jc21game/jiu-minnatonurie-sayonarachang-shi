/* タイトルシーン */

#pragma once
#include "Engine/Global.h"
#include "Engine/Base.h"
class TitleScene : public Base
{
	//画像名

	//カラー版
	cv::Mat imgColorTitle_ = cv::imread(IMG_COLORTITLE, 1);
	cv::Mat imgBack_	   = cv::imread(IMG_BACK, 1);

	//動画用Mat
	cv::Mat movie_;

	// 動画ファイルを取り込むためのオブジェクトを宣言する
	cv::VideoCapture video_;

	//動画用の変数
	// 作成する動画ファイルの設定
	int width_ = (int)video_.get(cv::CAP_PROP_FRAME_WIDTH);	// フレーム横幅を取得
	int height_ = (int)video_.get(cv::CAP_PROP_FRAME_HEIGHT);	// フレーム縦幅を取得
	double fps_ = video_.get(cv::CAP_PROP_FPS);				// フレームレートを取得


	//タイトルの設置位置？
	struct TitlePosition
	{
		const int POS_X = 0;
		const int POS_Y = 0;
		int CUT_X = 190;
		const int CUT_Y = 1024;
	}CutPosition;

public:
	//コンストラクタ
	TitleScene(Base* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

/*	動画参考：
	https://shizenkarasuzon.hatenablog.com/entry/2020/03/21/000437

*/