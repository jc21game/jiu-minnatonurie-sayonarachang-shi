
#pragma once
#include "Engine/Global.h"
#include "Engine/Input.h"
#include "Engine/Text.h"
#include "Engine/Base.h"

class SelectScene : public Base
{
	//シーンに必要な画像の読み込み
	cv::Mat imgPlay_   = cv::imread(IMG_PLAY, 1);	//全ての統合元となるMat
	cv::Mat imgRArrow_ = cv::imread(IMG_RARROW, 1);
	cv::Mat imgLArrow_ = cv::imread(IMG_LARROW, 1);
	cv::Mat imgReturn_ = cv::imread(IMG_RETURN, 1);
	cv::Mat imgTheme_  = cv::imread(IMG_THEME, 1);

	//イラスト：前ボタン
	//->構造体のメンバ変数で固定値(絶対の確定値)をとるときは、コンパイル時定数の"constexpr"
	//メンバ変数のときはstaticもセットで(Globalなどの時はいらない)
	//Uidataに合わせました	*千田
	const UiData backIllust_   = {  260,  300,  200,  400 };
	//イラスト：進むボタン
	const UiData frontIllust_  = { 1460,  300,  200,  400 };
	//選択するイラスト
	const UiData selectIllust_ = {  610,  190,  700,  700 };
	//戻るボタン
	const UiData returnScene_  = {   50,   50,  150,  150 };

	//イラスト変更カウント	*千田
	int illustCount_ = 0;

	bool clicknow_ = false;

public:
	//コンストラクタ
	SelectScene(Base* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};