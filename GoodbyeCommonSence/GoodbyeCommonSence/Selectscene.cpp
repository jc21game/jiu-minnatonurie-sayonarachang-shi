#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <Windows.h>

#include "Engine/SceneManager.h"
#include "Engine/Data.h"
#include "Selectscene.h"

SelectScene::SelectScene(Base * parent)
	:Base(parent,"SelectScene")
{
}

void SelectScene::Initialize()
{
	//画像統合
	//仮置きmat(いったん置かなければ使えなさそう？)
	cv::Mat img = Data::GetIllustMat(illustCount_);

	resize(
		img,
		img,
		cv::Size(),
		700.0 / img.cols,
		700.0 / img.rows );		//サイズ調整

	imgPlay_ = JoinImage(imgPlay_, img, selectIllust_.POS_X, selectIllust_.POS_Y);		//元絵配置
	imgPlay_ = JoinImage(imgPlay_, imgRArrow_, frontIllust_.POS_X, frontIllust_.POS_Y);			//右矢印配置
	imgPlay_ = JoinImage(imgPlay_, imgLArrow_, backIllust_.POS_X, backIllust_.POS_Y);				//左矢印配置
	imgPlay_ = JoinImage(imgPlay_, imgReturn_, 50, 50);					//戻るボタン配置
	imgPlay_ = JoinImage(imgPlay_, imgTheme_, 550, 45);				//お題背景配置

	//テキスト出力を使えるように *千田
	Text text;

	//最初のイラストのお題(仮置き) *千田
	text.SetDraw(imgPlay_, text.line_[text.lineNumber_], cv::Point(590, 100), "メイリオ", 10, cv::Scalar(0, 0, 0));
	text.Indention(Data::GetThemePath(illustCount_));



	//マウスセット
	cv::setMouseCallback(WIN_NAME, Input::mouse_callback, &mouseEvent);

	img.release();
}

void SelectScene::Update()
{
	//各ボタンの当たり判定準備(Updateに書かなければならないので、関数化、クラス化してしまった方がいい希ガス)
	cv::Rect backIllustCollider = cv::Rect(backIllust_.POS_X, backIllust_.POS_Y, backIllust_.SIZE_X, backIllust_.SIZE_Y);
	cv::Rect frontIllustCollider = cv::Rect(frontIllust_.POS_X, frontIllust_.POS_Y, frontIllust_.SIZE_X, frontIllust_.SIZE_Y);
	cv::Rect selectIllustCollider = cv::Rect(selectIllust_.POS_X, selectIllust_.POS_Y, selectIllust_.SIZE_X, selectIllust_.SIZE_Y);
	cv::Rect returnSceneCollider = cv::Rect(returnScene_.POS_X, returnScene_.POS_Y, returnScene_.SIZE_X, returnScene_.SIZE_Y);

	//もしパレット内だったら
	if (selectIllustCollider.contains(mouse))
	{
		//シーン遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		Data::illustNumber = illustCount_;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);

	}

	//イラスト変更処理
	if (Input::MouseLDown())
	{
		SetMousePos();

		//もし左矢印の範囲内だったら
		if (backIllustCollider.contains(mouse))
		{
			illustCount_--;
			//最初まで行ったら最後に行く
			if (illustCount_ < 0)
				illustCount_ = Data::MAX - 1;
		}

		//もし右矢印の範囲内だったら
		if (frontIllustCollider.contains(mouse))
		{
			illustCount_++;
			//最後まで行ったら最初に戻る
			if (illustCount_ >= Data::MAX)
				illustCount_ = 0;
		}

		//もし戻るボタンを押したら
		if (returnSceneCollider.contains(mouse))
		{
			//シーン遷移
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_TITLE);
		}

		//リセット用(今は仮置き)
		//一時リセット
		imgPlay_ = JoinImage(imgPlay_, imgTheme_, 550, 45);

		//結合した画像にお題表示
		Text text;
		text.SetDraw(imgPlay_, text.line_[text.lineNumber_], cv::Point(590, 100), "メイリオ", 10, cv::Scalar(0, 0, 0));
		text.Indention(Data::GetThemePath(illustCount_));

		//現在のイラストカウントのイラストを結び付け
		cv::Mat img = Data::GetIllustMat(illustCount_);

		//大きさ調整
		resize(
			img,
			img,
			cv::Size(),
			700.0 / img.cols,
			700.0 / img.rows );

		//画像統合
		imgPlay_ = JoinImage(
			imgPlay_,
			img,
			selectIllust_.POS_X,
			selectIllust_.POS_Y);

		img.release();
	}
}

void SelectScene::Draw()
{
	//画像表示
	cv::imshow(WIN_NAME, imgPlay_);
}

//解放処理
void SelectScene::Release()
{
	imgTheme_.release();
	imgReturn_.release();
	imgLArrow_.release();
	imgRArrow_.release();
	imgPlay_.release();

	for (int i = 0; i < Data::MAX; i++)
	{
		Data::GetIllustMat(i).release();
	}
}