#include "Input.h"

//マウス処理
void Input::mouse_callback(int event, int x, int y, int flags, void * userdata)
{
	//void*でない引数を取得
	Input::mouseParam *ptr = static_cast<Input::mouseParam*>(userdata);

	//各パラメータを結び付け
	ptr->event = event;
	ptr->x = x;
	ptr->y = y;
	ptr->flags = flags;


}

//マウスを押した瞬間の判定
bool Input::MouseLDown()
{
	static bool f = true;
	if (f && GetAsyncKeyState(VK_LBUTTON))
	{
		f = false;
		return true;
	}
	else if (!GetAsyncKeyState(VK_LBUTTON))
	{
		f = true;
	}
	return false;
}
