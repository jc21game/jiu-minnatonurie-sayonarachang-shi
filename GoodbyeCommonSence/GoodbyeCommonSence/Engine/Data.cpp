#include "Data.h"

namespace Data
{
	//イラストのデータ(パスうんぬん)
	struct illustData
	{
		cv::Mat illustMat;
		std::string textPath;
		std::string commentPath;
	}IllustData[MAX];

	//イラストパスの文字列
	//結び付ける文字列関連はcpp側に書いたほうがいい
	std::string illustList[MAX] =
	{
		IMG_CAT,
		IMG_HORSE,
		IMG_FIRECAT,
		IMG_CUPNOODLE,
		IMG_RAMEN,
		IMG_BOMBRABBIT,
		IMG_ICECREAM,
		IMG_ONION,
		IMG_SHARVEDICE,
		IMG_GAMERABBIT,
		IMG_SANTASHOES
	};

	//結び付けるお題のパス
	std::string titlePath[MAX] =
	{
		"Text/Theme/Practice.txt",
		"Text/Theme/Horse.txt",
		"Text/Theme/FireCat.txt",
		"Text/Theme/CupNoodle.txt",
		"Text/Theme/Ramen.txt",
		"Text/Theme/BombRabbit.txt",
		"Text/Theme/Icecream.txt",
		"Text/Theme/Onion.txt",
		"Text/Theme/SharvedIce.txt",
		"Text/Theme/GameRabbit.txt",
		"Text/Theme/SantaShoes.txt"
	};

	//結びつける評価のパス(一部)
	std::string comLittlePath[MAX] =
	{
		"Practice/",
		"Horse/",
		"FireCat/",
		"CupNoodle/",
		"Ramen/",
		"BombRabbit/",
		"Icecream/",
		"Onion/",
		"SharvedIce/",
		"GameRabbit/",
		"SantaShoes/"
	};


	//イラストパス、お題パスをIllustDataに格納(遠藤先生のと同じように)
	//=>stringの配列を準備して、格納するところでループ定義。
	//イラストのパスをMatで読み込み
	void SetIllustData()
	{
		for (int i = 0; i < MAX; i++)
		{
			IllustData[i].illustMat = cv::imread(illustList[i], 1);
			IllustData[i].textPath = titlePath[i];
			IllustData[i].commentPath = comLittlePath[i];
		}
	}

	//これらのデータを使う前に一回呼び出しちゃえば全シーンで同じデータが使える
	//=>SceneManagerのイニシャライズなどで呼び出しておけば使う先々でGeterを
	cv::Mat GetIllustMat(int illustCount)
	{
		return IllustData[illustCount].illustMat;
	}

	std::string GetThemePath(int illustCount)
	{
		return IllustData[illustCount].textPath;
	}

	std::string GetCommentPath(int illustCount)
	{
		return IllustData[illustCount].commentPath;
	}

	//準備した全部のMatの開放
	void Release()
	{
		for (int i = 0; i < MAX; i++)
		{
			GetIllustMat(i).release();
		}
	}

	//externの宣言
	//今遊んでいるイラストの保存パス
	std::string path;

	//今遊んでいるイラストの番号
	int illustNumber;

	//遊んだ時間
	int playTime;

	//もっとも使った色
	ColorPalette passingColor;

}