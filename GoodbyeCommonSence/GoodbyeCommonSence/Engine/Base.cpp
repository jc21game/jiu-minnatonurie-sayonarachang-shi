#include "Base.h"
#include "Global.h"

//何もなし
Base::Base(void)
	: Base(nullptr, "")
{
}

//標準
Base::Base(Base * parent, const std::string & name)
	: pParent_(parent), objectName_(name), IsDead_(false)
{
	//子供リスト初期化
	childList_.clear();

	//背景登録
	bImg = cv::imread(IMG_BACK);
}

//デストラクタ
Base::~Base()
{
	//imshowの解放処理
	KillAll();
}

//更新処理
void Base::UpdateSub()
{
	//自身のUpdate
	Update();

	//以下の子ども全てのUpdate呼び出し
	//子リストの中身をすべてアップデート
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->UpdateSub();
	}
	for (auto itr = childList_.begin(); itr != childList_.end();)
	{
		//子リストの中身(各オブジェクト)が死んでいるか(フラグ確認)
		if ((*itr)->IsDead_ == true)
		{
			//子の子たちをすべて消す
			(*itr)->ReleaseSub();

			//そのオブジェクトをdelete
			delete (*itr);

			//子供たちリストから削除
			//eraceの戻値で次のイテレーター指定(ここでリスト更新をしている)
			itr = childList_.erase(itr);
		}
		//eraceの更新がない場合、こちらで更新
		else
		{
			itr++;
		}
	}


}

//描画
void Base::DrawSub()
{
	//自身の描画
	Draw();

	//子供以下をすべてDraw
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->DrawSub();
	}

}

//解放
void Base::ReleaseSub()
{
	//自身の解放
	Release();

	//子供以下をすべてRelease
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->ReleaseSub();
	}

}

//オブジェクト(シーン)探索関数
Base * Base::FindObject(std::string name)
{
	//名前からオブジェクト探索
	return GetRootJob()->FindChildObject(name);
}

//子供以下のオブジェクト探索
Base * Base::FindChildObject(std::string name)
{
	//子供以下のオブジェクトをリストから名前で探索
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		if ((*itr)->objectName_ == name)
		{
			return *itr;
		}
		//再起処理(ループ)
		Base *obj = (*itr)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}
	}
	//見つからなければnullptr
	return nullptr;

}

//再親取得
Base * Base::GetRootJob()
{
	//親がいなければこれを返す
	if (pParent_ == nullptr)
	{
		return this;
	}
	else
	{
		//再起処理(ループ)
		return pParent_->GetRootJob();
	}
}

//全部殺す関数
void Base::KillAll()
{
	//自分の子供を全部消す
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->Release();
		delete(*itr);
	}
	//子リストクリア
	childList_.clear();

}

//画像を重ねて前方に表示
//引数：背景　前に出す画像　前方画像のx座標　前方画像のy座標
//戻値：表示するMat
cv::Mat Base::JoinImage(const cv::Mat & backImg, const cv::Mat & frontImg, const int tx, const int ty)
{
	//背景画像のMatをコピーして背景作成
	backImg.copyTo(bImg);

	//アフィン変換を使う行列
	cv::Mat mat = (cv::Mat_<double>(2, 3) << 1.0, 0.0, tx, 0.0, 1.0, ty);

	//アフィン変換
	warpAffine(frontImg, bImg, mat, bImg.size(), cv::INTER_LINEAR, cv::BORDER_TRANSPARENT);

	//表示
	imshow(WIN_NAME, bImg);

	//画像保存
	//画質を設定するparam(vectorで生成)
	std::vector<int> params(2);
	//型指定
	params[0] = cv::IMWRITE_JPEG_QUALITY;
	//画質指定
	params[1] = 100;

	//書き出し(保存)
	//imwrite("テスト.jpg", Bimg, params);

	//変換後のMatも使えるように返却(いらない)
	return bImg;
}

//マウスの座標をセット
void Base::SetMousePos()
{
	mouse.x = mouseEvent.x;
	mouse.y = mouseEvent.y;
}