#include "Timer.h"

#pragma comment(lib, "winmm.lib")

namespace Timer
{
    static auto deltaTime = std::chrono::microseconds(0);       //デルタタイム
    static float microDiv = 1000000.f;      //マイクロ

    double Timer::GetDelta()
    {
        return deltaTime.count() / microDiv;        //経過時間をマイクロ秒から秒に変換してから返す
    }

    //初期化
    void Timer::Reset()
    {
        deltaTime = std::chrono::microseconds(0);
    }

    void Timer::UpdateFrameDelta()
    {
        static auto prevTime = std::chrono::system_clock::now();

        auto now = std::chrono::system_clock::now();

        deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - prevTime);
    
        prevTime = now;
    }

	double EndPlayTime(clock start, clock end)
	{
		return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / microDiv;
	}
}