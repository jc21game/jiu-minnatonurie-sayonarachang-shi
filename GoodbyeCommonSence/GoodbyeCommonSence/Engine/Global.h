/* 全体で使う文字列関係のもの */

#pragma once

//ウィンドウ名(全体で呼べるようにグローバル化)
#define WIN_NAME  "TEST"

//シーン
#define IMG_SPLASH		 "Texture/TeamRogo_.png"
#define IMG_TITLE		 "Texture/TITLE.png"
#define IMG_COLORTITLE	 "Texture/TitleColor.png"
#define IMG_PLAY		 "Texture/PLAY.jpg"


//画像名
#define IMG_BACK		 "Texture/Base.png"
#define IMG_BASE_PALETTE "Texture/BasePalette.png"
#define IMG_RARROW		 "Texture/RArrow.png"
#define IMG_LARROW		 "Texture/LArrow.png"
#define IMG_SPUIT		 "Texture/ChangeMode.png"
#define IMG_SAVE_PALETTE "Texture/SavePalette.png"
#define IMG_BUCKET		 "Texture/Bucket.jpg"
#define IMG_MODE_SPUIT   "Texture/SpuitMode.png"
#define IMG_MODE_BUCKET  "Texture/BucketMode.png"

#define IMG_RETRY	 "Texture/retryiconAdframe.png"
#define IMG_SAVE	 "Texture/saveiconAdframe.png"
#define IMG_COMPLETE "Texture/completeiconAdframe.png"
#define IMG_HOME	 "Texture/homeAdframe.png"
#define IMG_RETURN	 "Texture/returniconAdframe.png"
#define IMG_THEME	 "Texture/ThemeAdframe.png"
#define IMG_TURTLE	 "Texture/turtleAdframe2.png"
#define IMG_RABBIT	 "Texture/rabbitAdframe.png"
#define IMG_COMMENT  "Texture/Comment_frame.png"


//後で差し替え
#define IMG_SELECTED_PAINT "Texture/SelectedPaint.png"

//イラスト
#define IMG_CAT			"Texture/Illust/testcat.png"
#define IMG_HORSE		"Texture/Illust/horse.png"
#define IMG_FIRECAT		"Texture/Illust/firecat.bmp"
#define IMG_CUPNOODLE	"Texture/Illust/noodle.bmp"
#define IMG_RAMEN		"Texture/Illust/ramen.bmp"
#define IMG_BOMBRABBIT	"Texture/Illust/bombrabbit.png"
#define IMG_ICECREAM	"Texture/Illust/icecream.png"
#define IMG_ONION		"Texture/Illust/onion.png"
#define IMG_SHARVEDICE  "Texture/Illust/sharvedice.png"
#define IMG_GAMERABBIT	"Texture/Illust/GamingUsa.png"
#define IMG_SANTASHOES  "Texture/Illust/SantaShoes.png"

//waitkeyで返されるASCIIコード
#define KEY_SPACE	32
#define KEY_ENTER	13

//チュートリアル
#define IMG_TUTORIAL	 "Texture/tutorial.png"
#define IMG_TUTORIAL_2	 "Texture/tutorial2.png"

/* 使わなくなったもの */
//#define IMG_PARET	 "Texture/Paret.png"
//#define IMG_PARET2	 "Texture/Paret2.png"
//#define IMG_SHARE	 "Texture/Share.png"
//#define IMG_RETRY	 "Texture/Retry.png"
//#define IMG_SAVE	 "Texture/Save.png"
//#define IMG_COMPLETE "Texture/Complete.png"
//#define IMG_HOME	 "Texture/Home.png"
//#define IMG_RETURN	 "Texture/Return.png"
//#define IMG_THEME	 "Texture/Theme.png"
//#define IMG_TURTLE	 "Texture/Turtle.png"
//#define IMG_RABBIT	 "Texture/Rabbit.png"


/*	メモ *千田
	opencv参照:https://docs.opencv.org/master/d3/d63/classcv_1_1Mat.html#aa5d20fc86d41d59e4d71ae93daee9726

*/