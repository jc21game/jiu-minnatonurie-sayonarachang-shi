/* シーン遷移を管理するオブジェクト */

#pragma once
#include "Base.h"

//各シーンID
enum SCENE_ID
{
	SCENE_ID_SPLASH = 0,
	SCENE_ID_TITLE,
	SCENE_ID_TUTORIAL,
	SCENE_ID_TUTORIAL_2,
	SCENE_ID_SELECT,
	SCENE_ID_PLAY,
	SCENE_ID_RESULT,
};

class SceneManager : public Base
{
public:
	//コンストラクタ
	SceneManager();
	//コンストラクタ(親あり)
	SceneManager(Base* parent);
	//デストラクタ
	~SceneManager();

	//現在のシーンID
	SCENE_ID currentSceneID_;
	//次のシーンID
	SCENE_ID nextSceneID_;

	//初期処理
	void Initialize() override;
	//更新処理
	void Update() override;
	//描画処理
	void Draw() override;
	//解放処理
	void Release() override;

	//シーン切替
	void ChangeScene(SCENE_ID next);

};

