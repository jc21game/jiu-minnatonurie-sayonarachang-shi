#include "MatSave.h"
namespace MatSave
{
	std::vector<MatData*> md;

	int Load(std::string filename)
	{
		MatData* pMD = new MatData;
		pMD->matName = filename;

		for (int i = 0; i < (int)md.size(); i++)
		{
			if (md[i]->matName == pMD->matName)
			{
				pMD->pMat = md[i]->pMat;
				md.push_back(pMD);

				return (int)md.size() -1;
			}
		}
		
		if (cv::imread(pMD->matName,1).data != NULL)
		{
			pMD->mat = cv::imread(pMD->matName, 1);
			md.push_back(pMD);
			return (int)md.size() - 1;
		}
		return -1;
	}

	cv::Mat CopyImage(int TextureNumber_)
	{
		return md[TextureNumber_]->mat;
	}

	void Release()
	{
		for (int i = 0; i < (int)md.size(); i++)
		{
			for (int j = 0; j < i; j++)
			{
				if (md[i]->matName == md[j]->matName)
				{
					continue;
				}
				md[i]->pMat->release();
				if (md[i]->pMat != nullptr)
				{
					delete(md[i]->pMat);
					md[i]->pMat = nullptr;
				}
			}
		}

		for (int i = 0; i < (int)md.size(); i++)
		{
			if (md[i] != nullptr) {
				delete(md[i]);
				md[i] = nullptr;
			}
		}
		md.clear();
	}

	

}