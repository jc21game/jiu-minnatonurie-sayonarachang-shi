#include "Text.h"

//コンストラクタ
Text::Text()
{
	//各メンバ初期化
	ZeroMemory(line_, sizeof(line_));
	//=>初期化によって文字化けが起こるので初期化の方法が間違っている？
	img_ = 0;
	//ZeroMemory(text_,sizeof(text_));
	//orgの初期化法が不明
	fontName_ = nullptr;
	fontScale_ = 0;
	color_ = 0;

}

//デストラクタ
Text::~Text() {}

//日本語表示
//引数：画像Mat、表示文字、cvの座標、フォント名、フォントサイズ、色情報
void Text::drawText(cv::Mat& img, const cv::String& text, const cv::Point& org, const char* fontname, double fontScale, cv::Scalar color)
{
	//実際に表示するフォントサイズ
	int fontSize = (int)(10 * fontScale);
	//文字幅
	int width = img.cols;
	//文字高さ(ある程度増やさないと全表示できない)
	int height = fontSize * 3 / 2;

	//指定されたデバイスと互換性のあるメモリデバイスコンテキスト(DC)を作成
	HDC hdc = ::CreateCompatibleDC(NULL);

	//DIB(画像形式)の寸法とカラーフォーマットについての情報(構造体)
	//表示文字を画像に出して、その画像を出力するカタチ
	BITMAPINFOHEADER header;
	::ZeroMemory(&header, sizeof(BITMAPINFOHEADER));
	//構造体に必要なバイト数
	header.biSize = sizeof(BITMAPINFOHEADER);
	//ビットマップの幅
	header.biWidth = width;
	//ビットマップの高さ、原点は左下
	header.biHeight = height;
	//ターゲットデバイスのプレーン数(?)とりあえず1にするものらしい
	header.biPlanes = 1;
	//ピクセル当たりのビット数
	header.biBitCount = 24;

	//BITMAPINFOHEADERに似たもの？
	//上記のHEADERに色情報を加えたもの
	BITMAPINFO bitmapInfo;
	//BITMAPINFOHEADERのカラーフォーマットの大きさに関する情報を含む構造体
	bitmapInfo.bmiHeader = header;
	//アプリケーションが直接書き込むことのできるDIBの作成
	//=>ピクセル単位でcvに直接書き込めるように
	HBITMAP hbmp = ::CreateDIBSection(
		NULL,						//DCへのハンドル
		(LPBITMAPINFO)&bitmapInfo,	//BITMAPINFO構造体へのポインタ
		DIB_RGB_COLORS,				//リテラルRGB値(記述可能なRGB)の配列(定義されたもの)
		NULL,						//DIBビット値の場所へのポインタを受け取る変数へのポインタ
		NULL,						//共有記憶領域へのハンドル(NULL可)
		0							//共有記憶領域の先頭からの幅(NULL可)
	);
	//指定されたDCに変換するオブジェクトを選択
	::SelectObject(hdc, hbmp);

	//ビットマップのカプセル化
	BITMAP  bitmap;
	//指定されたグラフィックオブジェクトの情報を取得
	//引数：グラフィックオブジェクトのハンドル、バッファに格納されるバイト数、1個のバッファへのポインタ(BITMAP構造体のアドレス)
	::GetObject(hbmp, sizeof(BITMAP), &bitmap);

	//背景色
	int back_color = 0x99;
	//メモリサイズ
	//bmBitsPixel：ビットマップの1ピクセルを表すのに使用するビット数
	//((ビットマップの1ピクセルを表すのに使用するバイト数 * 幅) & NOT3) * 高さ
	//=>& ~3は消しても通る
	int memSize = ((bitmap.bmBitsPixel / 8) * width * height);
	//メモリに指定のバイト数分の値をセット
	//=>色値を文字列表示の画像の大きさ分セットする
	//引数：メモリのポインタ、セットする値、セットするサイズ
	std::memset(bitmap.bmBits, back_color, memSize);

	//指定されたフォントを生成する
	HFONT hFont = ::CreateFontA(
		fontSize,					//文字の高さ
		0,							//文字の幅
		0,							//エスケープメントベクトルとデバイスのx軸の間の10分の1度単位の角度
		0,							//各キャラクターのベースラインとデバイスのx軸の間の10分の1度単位の角度
		FW_DONTCARE,				//フォントの太さ
		FALSE,						//イタリックフォントON/OFF
		FALSE,						//下線ON/OFF
		FALSE,						//取り消し線ON/OFF
		SHIFTJIS_CHARSET,			//文字セット
		OUT_DEFAULT_PRECIS,			//出力精度
		CLIP_DEFAULT_PRECIS,		//クリッピングの制度
		DEFAULT_QUALITY,			//出力品質
		VARIABLE_PITCH | FF_ROMAN,	//フォントのピッチとファミリ
		fontname					//Windows内にあるフォント名(ＭＳ ゴシックなど)
	);
	//指定されたDCに変換するオブジェクトを選択
	::SelectObject(hdc, hFont);

	//以下で記述処理

	//現在選択されているフォント、背景色、およびテキストの色を使用して、指定された位置に文字列を書き込む
	::TextOutA(
		hdc,				//DCへのハンドル
		0,					//x座標
		height / 3,			//y座標
		text.c_str(),		//描画する文字列へのポインタ(Char型)
		(int)text.length()	//文字列の長さ
	);




	//cv側の座標
	int posX = org.x;
	int posY = org.y - fontSize;


	//一時的な作業ファイル
	unsigned char* _tmp;
	//画像の色情報が入る
	//0〜255の色情報しか入らないのでchar、というかcharでなければ1バイト刻みで見られない
	unsigned char* _img;

	//フォントの入る画像をその画像サイズ分で描画(再現)
	//ビットマップ単位での高さの取得
	for (int y = 0; y < bitmap.bmHeight; y++) {
		if (posY + y >= 0 && posY + y < img.rows) {
			//ビット、ピクセル毎にイメージ(文字)色の割り出し。多分、論理式を用いて文字を切り抜くように(くりぬくように)出力している
			//bitmap.bmBitsはビットイメージの先頭
			//各img、tmpの計算に& ~3が記述されていたが、なくても動作する(数値に変化がない)
			_img = img.data + (int)(3 * posX + (posY + y) * (((bitmap.bmBitsPixel / 8) * img.cols)));
			_tmp = (unsigned char*)(bitmap.bmBits) + (int)((bitmap.bmHeight - y - 1) * (((bitmap.bmBitsPixel / 8) * bitmap.bmWidth)));
			//ビットマップ単位での幅の取得
			for (int x = 0; x < bitmap.bmWidth; x++) {
				if (x + posX >= img.cols) {
					break;
				}
				//色情報の格納、RGB数値をピクセル毎に入れている(格納は左下から順に)
				if (_tmp[0] == 0 && _tmp[1] == 0 && _tmp[2] == 0) {
					_img[0] = (unsigned char)color.val[0];
					_img[1] = (unsigned char)color.val[1];
					_img[2] = (unsigned char)color.val[2];
				}
				//ピクセルは3バイト境界なので3バイト進めば次のピクセルを参照する
				_img += 3;
				_tmp += 3;
			}
		}
	}



	//生成していったものの削除
	::DeleteObject(hFont);
	::DeleteObject(hbmp);
	::DeleteDC(hdc);
}

//改行処理
void Text::Indention(std::string textPass)
{
	std::ifstream ifs(textPass);

	//テスト
	//行単位で文字列読み込み
	while (std::getline(ifs, readString_))
	{
		//ファイルの終わりまで一行ずつ読み込み
		if (readString_.find(','))
		{
			//読み込む文字列の大きさ
			stringSize_ = readString_.length();
			//読む範囲(改行まで)
			line_[lineNumber_] = readString_.substr(0, readString_.find(','));
			text_[lineNumber_] = line_[lineNumber_];
			//一行表示
			drawText(img_, text_[lineNumber_], org_, fontName_, fontScale_, color_);

			//範囲詰め

			readString_ = readString_.substr(readString_.find(',') + 1, stringSize_ - readString_.find(','));
			lineNumber_++;
			org_.y += 100;

		}
	}

	//ファイル閉じ
	ifs.close();
}

//セッター
void Text::SetDraw(cv::Mat& img, const cv::String& text, const cv::Point& org, const char* fontName, double fontScale, cv::Scalar color)
{
	//各項目結び付け
	img_ = img;
	text_[lineNumber_] = text;
	org_ = org;
	fontName_ = fontName;
	fontScale_ = fontScale;
	color_ = color;
}


/*	メモ
	hFontでメモリが読み取れずエラー
	=>日本語のfontNameが読み込めてない？フォント名を読み込ませるためのパスを通すか、
	定義したら通るのか要検討

	11/17
	CreateFont内部で文字列が読み込まれない(登録されていない文字フォント名)だった場合、入力文字列に適したフォントが
	返されるらしい(フォントを探して最初に見つかったもの)。なのでその場合漢字とひらがな・カタカナでフォントが異なってしまう
	=>分かりやすいのは"ＭＳ ゴシック"など。ＭＳは全角なので注意

	opencvのputTextでは引数のfontFaceでcv側で列挙定義されたFontを使っていた
	このdrawTextはfontFaceの枠にWin32の機能であるCreateFontを噛ませて日本語に対応させる形にしたもの

	11/19
	改行にも対応させる必要あり、テキストエディタに対応させる必要も
	=>テキストエディタには文字コードを[ANSI]にすることで表示ができた
	が、改行は叶わなかったので要修正
	=><fstream>をインクルードし、getlineを追加することでテキスト読み込み、改行に成功

	関数分けした時、配列にて文字列読み込む文字列、書き込む文字列の範囲を管理していたので、配列の引き渡しが上手くいかず
	表示できていない
	=>main側にループ文を記述する必要アリ？
	=>改行処理に記述することで解決

	フォントを変更することで大きさも異なる、多分フォントはデザイン的にも統一すべきかも
	フォント一覧=>http://kako-g.com/font-list/


	参考資料：http://eternalwindows.jp/graphics/bitmap/bitmap09.html
*/