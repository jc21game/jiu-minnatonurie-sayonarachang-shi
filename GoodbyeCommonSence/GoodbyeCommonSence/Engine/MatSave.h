#pragma once
#include "Base.h"

namespace MatSave
{
	struct MatData
	{
		cv::Mat mat;
		std::string matName;
		cv::Mat* pMat;
		MatData() : pMat(nullptr)
		{
		}
	};

	int Load(std::string filename);

	cv::Mat CopyImage(int TextureNumber_);

	void Release();
}