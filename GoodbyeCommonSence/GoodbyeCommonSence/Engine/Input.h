/* マウス入力を結びづける構造体 */

#pragma once
//#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <Windows.h>

//マウスパラメータの構造体
namespace Input 
{
	//マウスパラメータ
	struct mouseParam
	{
	public:
		int x;
		int y;
		int event;
		int flags;
	};

	//マウス処理
	void mouse_callback(int event, int x, int y, int flags, void *userdata);

	//左クリック判定
	bool MouseLDown();

}
