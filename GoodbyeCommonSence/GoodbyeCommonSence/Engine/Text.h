#pragma once

#include <opencv2/opencv.hpp>
#include <Windows.h>
#include <fstream>
#include <string>
#include <iostream>


class Text
{
public:
	Text();
	~Text();

	//読み込む文字列の大きさ
	size_t stringSize_;
	//読み込むのに使う文字列
	std::string readString_;
	//各行が入る文字列
	std::string line_[3];
	//配列の添え字(仮)
	int lineNumber_;

	//以下セッターとして結びつけるデータ
	cv::Mat img_;
	cv::String text_[3];	//改行は二行まで
	cv::Point org_;
	const char* fontName_;
	double fontScale_;
	cv::Scalar color_;

	//テキストの描画
	//引数：文字を入れるMat、描画テキスト、座標、フォント名、大きさ、色
	void drawText(cv::Mat& img, const cv::String& text, const cv::Point& org, const char* fontname, double fontScale, cv::Scalar color);
	//opencvの関数に似せた引数をセットする関数
	void SetDraw(cv::Mat& img, const cv::String& text, const cv::Point& org, const char* fontName, double fontScale, cv::Scalar color);
	//改行処理
	void Indention(std::string textPass);

};