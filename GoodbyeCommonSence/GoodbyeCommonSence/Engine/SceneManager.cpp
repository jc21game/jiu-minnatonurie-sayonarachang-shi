#include "SceneManager.h"
#include "Data.h"

//各シーンをinclude
#include "../TitleScene.h"
#include "../PlayScene.h"
#include "../SplashScene.h"
#include "../Selectscene.h"
#include "../ResultScene.h"
#include "../TutorialScene.h"
#include "../TutorialScene2.h"

//コンストラクタ
SceneManager::SceneManager()
{
}

//コンストラクタ(親あり)
SceneManager::SceneManager(Base * parent)
	: Base(parent,"SceneManager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//シーン切り替え
void SceneManager::ChangeScene(SCENE_ID next)
{
	//シーン切り替え(セッターと同義)
	nextSceneID_ = next;
}

//初期処理
void SceneManager::Initialize()
{
	//すべてのシーンの親にあたるので、ここで各シーンで扱うイラストのデータをセット
	Data::SetIllustData();

	//一番最初のシーン準備
	currentSceneID_ = SCENE_ID_SPLASH;
	nextSceneID_ = currentSceneID_;

	//名前のシーン(オブジェクト)生成
	Instantiate<SplashScene>(this);
}

//更新処理
void SceneManager::Update()
{
	//シーン切り替え処理(currentとnextが違ったら)
	if (currentSceneID_ != nextSceneID_)
	{
		//子供以下解放処理
		KillAll();


		//シーン切り替え
		switch (nextSceneID_)
		{
		case SCENE_ID_SPLASH: Instantiate<SplashScene>(this); break;
		case SCENE_ID_TITLE: Instantiate<TitleScene>(this); break;
		case SCENE_ID_TUTORIAL: Instantiate<TutorialScene>(this); break;
		case SCENE_ID_TUTORIAL_2: Instantiate<TutorialScene2>(this); break;
		case SCENE_ID_SELECT: Instantiate<SelectScene>(this); break;
		case SCENE_ID_PLAY: Instantiate<PlayScene>(this); break;
		case SCENE_ID_RESULT: Instantiate<ResultScene>(this); break;
		}

		//現在のシーンに登録
		currentSceneID_ = nextSceneID_;

	}
}

void SceneManager::Draw()
{
}

//解放処理
void SceneManager::Release()
{
}

