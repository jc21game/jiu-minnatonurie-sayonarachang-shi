#pragma once
#include <chrono>
#define clock std::chrono::time_point<std::chrono::system_clock>

namespace Timer
{
	//フレームのデルタタイム
	double GetDelta();

	//シーンタイマーのリセット
	void Reset();

	//デルタタイムの更新
	void UpdateFrameDelta();

	//プレイ時間決定
	double EndPlayTime(clock start, clock end);
};

