/*各シーンの基底オブジェクト*/

//警告の削除
#pragma warning(disable : 4819)


#pragma once
#include <opencv2\opencv.hpp>
#include <string>
#include <list>
#include "Input.h"

//using namespace cv;

class Base
{
	//画像名
	std::string objectName_;

	//消えているかどうか
	bool IsDead_;

protected:
	//親オブジェクト(いらないかも)
	Base* pParent_;
	std::list <Base*> childList_;

public:
	//コンストラクタ
	Base();
	//名前ありコンストラクタ。画像名を参照で指定する場合、手動で解放(Mat::Release)
	Base(Base* parent, const std::string& name);

	//デストラクタ
	virtual	~Base();

	//各オブジェクトに持たせる関数
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;

	//子供以下にやる関数
	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	//オブジェクトを探す関数
	Base* FindObject(std::string name);
	Base* FindChildObject(std::string name);

	//最親取得
	Base* GetRootJob();

	//殺す
	void KillMe() { IsDead_ = true; }
	//ぜんぶ殺す
	void KillAll();

	//画像統合関数
	cv::Mat JoinImage(const cv::Mat &backImg, const cv::Mat &frontImg, const int tx, const int ty);

	//背景Mat(全シーン共通)
	cv::Mat bImg;

	//画像生成を行うテンプレート
	template <class T>
	Base* Instantiate(Base* pParent)
	{
		T* p = new T(pParent);
		p->Initialize();
		childList_.push_back(p);
		return p;
	}

	Input::mouseParam mouseEvent;

	//マウスの座標を管理
	cv::Point mouse;

	//担当者：山田
	//マウスの座標をセット
	//引数：なし
	//戻値：なし
	void SetMousePos();

	//UIの位置、大きさを管理(使用先で定数化)
	struct UiData
	{
		int POS_X;
		int POS_Y;
		int SIZE_X;
		int SIZE_Y;
	};
};

