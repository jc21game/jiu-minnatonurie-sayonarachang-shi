#pragma once
/* イラスト番号とイラストのパスとお題を結びつけるオブジェクト */

#include <opencv2/opencv.hpp>
#include <string>
#include "Global.h"

//色の列挙(各シーンにて使うので追記のないよう今はここに置きます)
enum ColorPalette
{
	RED,
	ORANGE,
	YELLOW,
	GREEN,
	BLUE,
	PURPLE,
	PINK,
	BROWN,
	GRAY,
	WHITE,
	BLACK,
	COLORMAX
};

namespace Data
{

	//遊べるイラストの列挙
	enum IllustName
	{
		CAT,
		HORSE,
		FIRECAT,
		CUPNOODLE,
		RAMEN,
		RABBIT,
		ICECREAM,
		ONION,
		SHARVEDICE,
		GAMERABBIT,
		SANTASHOES,
		MAX
	};

	//パス関連のセット
	void SetIllustData();

	//イラストMatの取得
	//引数：イラスト番号
	//戻値：imreadされたイラストのMat
	cv::Mat GetIllustMat(int illustCount);

	//お題のパス文字列の取得
	//引数：イラスト番号
	//戻値：イラストのお題パス
	std::string GetThemePath(int illustCount);

	//評価コメント時に使うパスの一部の文字列
	//引数：イラスト番号
	//戻値：評価パスの一部(イラストの名前部分)
	std::string GetCommentPath(int illustCount);

	//登録Matの解放
	void Release();

	//保存イラストのパス
	//extern=>宣言になる
	extern std::string path;

	//遊んでいるイラストの番号
	extern int illustNumber;

	//遊んだ時間
	extern int playTime;

	//リザルトに渡す評価元の色(名前被り防止で変えてます)
	extern ColorPalette passingColor;

}

/*  メモ *千田
	画像を追加していくときは、列挙の後尾に名前を追加して、cpp側にお題のパスも追加してください。
	cpp側のSetIllustDataで列挙から画像のパスを読み込んでimreadを行い、お題のパスにも結びつけます。
	シーン先で使うときはそれぞれ対応するゲッター、または変数を使ってください。
	シーン間で受け渡したい変数がある場合はここで行ってください。
*/