/* 全ての親元となるオブジェクト */

#pragma once
#include "Base.h"

class RootJob : public Base
{
public:
	RootJob();
	~RootJob();

	void Initialize() override;

	void Update() override;

	void Draw() override;

	void Release() override;
};

