//　担当者　奥山　,　伊藤

#include "Engine/SceneManager.h"//シーンマネージャー
#include "Engine/Global.h"//画像
#include "TutorialScene.h"

//コンストラクタ
TutorialScene::TutorialScene(Base * parent)
	:Base(parent, "TutorialScene")
{
}

//初期処理
void TutorialScene::Initialize()
{

}

//更新処理
void TutorialScene::Update()
{
	if (Input::MouseLDown())
	{
		//チュートリアル2シーン遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TUTORIAL_2);
	}
}

void TutorialScene::Draw()
{
	//描画
	imshow(WIN_NAME, imgTutorial_);

}

//解放処理
void TutorialScene::Release()
{
	imgTutorial_.release();	
}
