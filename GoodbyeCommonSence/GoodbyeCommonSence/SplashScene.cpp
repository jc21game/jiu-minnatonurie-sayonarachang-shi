#include <opencv2/opencv.hpp>
#include <cmath>
#include <chrono>

#include "Engine/SceneManager.h"
#include "SplashScene.h"

//#include "Engine/Timer.h"

//コンストラクタ
SplashScene::SplashScene(Base * parent)
	:Base(parent, "SplashScene"),alpha_Change_(0.05)
{
}

//初期処理
void SplashScene::Initialize()
{
}


//更新処理
void SplashScene::Update()
{
//フェードイン・フェードアウト
	static short count = 0;
	if(count < 20) beta_ -= alpha_Change_;		//背景のalpha値を減少
	if(count > 40) beta_ += alpha_Change_;		//背景のalpha値を増加

	//画像を加工
	addWeighted(coverPict_, beta_, basePict_, alpha_, 0.0, dst_);


	//少しずつ更新
	//マウス左クリックを押したらスキップ
	//キーボード長押しで連続でシーン切り替え可能だったため変更
	if (Input::MouseLDown() || count > 60)
	{
		//シーン切り替え
		//切り替えの処理はUpdate側に
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
	count++;
}

void SplashScene::Draw()
{
	//画像表示
	cv::imshow(WIN_NAME, dst_);

}

//解放処理
void SplashScene::Release()
{
	//解放
	dst_.release();
	coverPict_.release();
	basePict_.release();
}