/* プレイシーン */

#pragma once
#include <iostream>
#include <vector>
#include "Engine/Global.h" 
#include "Engine/Input.h"
#include "Engine/Timer.h"
#include "Engine/Text.h"
#include "Engine/Base.h"
#include "Engine/Data.h"

class ResultScene;
class SceneManager;

class PlayScene : public Base
{
	cv::Mat imgPlay_		= cv::imread(IMG_PLAY, 1);
	cv::Mat imgIllust_;
	cv::Mat imgBack_		= cv::imread(IMG_BACK);
	cv::Mat imgPallet_		= cv::imread(IMG_BASE_PALETTE, 1);
	cv::Mat imgHome_		= cv::imread(IMG_HOME, 1);
	cv::Mat imgComplete_	= cv::imread(IMG_COMPLETE, 1);
	cv::Mat imgBucket_		= cv::imread(IMG_BUCKET, 1);
	cv::Mat imgChangeMode_  = cv::imread(IMG_SPUIT, 1);
	cv::Mat imgSaveParet_	= cv::imread(IMG_SAVE_PALETTE, 1);
	cv::Mat imgTheme_		= cv::imread(IMG_THEME, 1);
	cv::Mat imgSpuitMode_	= cv::imread(IMG_MODE_SPUIT, 1);
	cv::Mat imgBucketMode_	= cv::imread(IMG_MODE_BUCKET, 1);

	enum PlayMode
	{
		PAINT,
		SPUIT
	};
	PlayMode mode_ = PAINT;

	cv::Vec3b pixData_;
	cv::Vec3b saveColorData;	//スポイトで保存する色情報を格納

	int selectedPaintPosY_;
	////保存されている色を0〜3で数える
	//int savedColorCount_ = 0;

	//スタートタイム
	clock startTime_;

	ColorPalette mostUsedColor_;

	//色の定義
	std::map<ColorPalette, std::vector<cv::Vec3b>> color_;

	//各UIの位置、大きさ
	const UiData illust_	 = {  610,  190,  699,  699 };
	const UiData palette_	 = { 1540,  190,  150,  770 };
	const UiData home_		 = {  230,  890,  150,  150 };
	const UiData complete_	 = { 1540,  890,  150,  150 };
	const UiData bucket_	 = { 1740,  900,  150,  150 };
	const UiData changeMode_ = { 1340,  900,  150,  150 };
	const UiData saveParet_	 = {  230,  190,  150,  283 };
	const UiData theme_		=  {  550,   45,  800,  100 };
	//const UiData theme = {550,45,800,100};	//大きさを指定すると、その分の座標を確保するみたいでずれが生じました、後で修正します

	const int R_PAINTS_COUNT_ = 4;
	const int L_PAINTS_COUNT_ = 3;

public:
	//コンストラクタ
	PlayScene(Base* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画処理
	void Draw() override;

	//開放
	void Release() override;

	//担当者：山田
	//選択した座標の色情報を取得し格納
	//引数：クリックした座標値、情報の格納先のピクセル
	//戻値：なし
	cv::Vec3b GetPixDatas(int x, int y);

	//担当者：山田
	//指定した色で塗りつぶす(floodFillへパラメタを渡して塗りつぶす)
	//引数：塗りつぶす画像、塗りつぶしの開始座標、塗りつぶす色
	//戻値：なし
	void FillWithColor(cv::Mat img, int startX, int startY, cv::Vec3b colorData);

	//担当者：山田
	//クリックした位置がパレットの絵の具の部分かどうか
	//引数：判定したいパレットが持っている絵具の数
	//戻値：パレット内だった：true / パレット外だった：false
	bool IsPalette(int paintsCount);

	//担当者：山田
	//選択している色が分かるように表示
	//引数：なし
	//戻値：なし
	void DispSelectedColor();

	//担当者 : 櫻庭
	//クリックした場所が線画かどうか
	//引数 : クリックした座標値
	//戻値 : 線画だった : ture / 線画じゃなかった : false
	bool IsLineArt(int x, int y);

	//担当者：千田
	//画像切り抜き
	//引数：対象のMat(プレイシーン全体)
	//戻値：切り取った画像
	cv::Mat Cutout(cv::Mat mat, std::string* path);

	//担当者：櫻庭
	//キャンバス部分の最も使われている色の取得
	//引数 : なし
	//戻値 : なし
	void GetMostUsedColor();

	//担当者 : 櫻庭
	//色の定義
	//引数 : なし
	//戻値 : なし
	void InitializeColor();

	//担当者 : 櫻庭
	//イラストの二値化
	//引数 : 二値化したい画像
	//戻値 : 二値化した画像
	cv::Mat ImageThresholding(cv::Mat mat);

	//担当者 : 櫻庭
	//色の評価
	//引数 :無し
	//戻値 : 評価コメント
	cv::String EvaluateColor();

	//担当者 : 櫻庭
	//一番使われている色の判別
	//引数 :　色の判定の緩さ(上限値、下限値)
	//戻値 : 一番使われている色
	ColorPalette IdentifyColor(cv::Vec3b judgeColor);

	//担当者 : 櫻庭
	//色の合成
	//引数 : 塗る色、クリックした座標
	//戻値 : 合成した色
	cv::Vec3b MixColor(cv::Vec3b mixcolor, int x, int y);
};