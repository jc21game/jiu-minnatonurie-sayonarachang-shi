/* スプラッシュシーン */

#pragma once
#include "Engine/Global.h"
#include "Engine/Base.h"

class SplashScene : public Base
{
	//画像名(読み込まれない)
	cv::Mat basePict_  = cv::imread(IMG_SPLASH, 1);
	cv::Mat coverPict_ = cv::imread(IMG_BACK, 1);

	//出力用の行列
	cv::Mat dst_;

	//アルファ値の設定
	double alpha_ = 1.0;
	double beta_ = 1.0;

	//alpha値増減
	const double alpha_Change_;

public:
	//コンストラクタ
	SplashScene(Base* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};